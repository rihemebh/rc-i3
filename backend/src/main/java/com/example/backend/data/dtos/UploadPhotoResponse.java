package com.example.backend.data.dtos;

import lombok.Data;

@Data
public class UploadPhotoResponse {


    private String fileName;
    private String fileLink;


}
