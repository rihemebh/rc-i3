import { Base_URL } from '../utils/constants';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginDTO } from '../dtos/login.dto';

@Injectable({providedIn: 'root'})
export class AdminService {
    constructor(private httpClient: HttpClient) { }
    

   public getData() :  Observable<any>{
    return this.httpClient.get(`${Base_URL}/products`);
   }
}