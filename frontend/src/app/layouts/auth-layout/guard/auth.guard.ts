import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';


@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router) {

  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    if (localStorage.getItem('token')==null) {
      console.log('yesss')
      this.router.navigateByUrl('/login')
      return false
    }
    return true
  }

}