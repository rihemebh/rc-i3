import { ModalComponent } from './../../components/modal/modal.component';
import { AvatarModule } from 'ngx-avatar';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ClipboardModule } from 'ngx-clipboard';

import { AdminLayoutRoutes } from './admin-layout.routing';
import { TablesComponent } from '../../pages/tables/tables.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthentificationInterceptorProvider } from '../auth-layout/interceptor/auth.interceptor';
// import { ToastrModule } from 'ngx-toastr';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AvatarModule,
    ClipboardModule,
    NgbModule,
  
  ],
  declarations: [
 
    TablesComponent,
    ModalComponent
    
  ],  providers: [AuthentificationInterceptorProvider],
})

export class AdminLayoutModule {}
