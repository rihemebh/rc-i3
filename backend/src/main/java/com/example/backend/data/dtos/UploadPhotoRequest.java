package com.example.backend.data.dtos;

import lombok.Data;

@Data
public class UploadPhotoRequest {
    private int id;
    private String fileType;
    private String fileName;

    private byte[]grpData;
}
