package com.example.backend.data.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity(name="Products")
@AllArgsConstructor
@Data
@NoArgsConstructor
public class Product {

    @Id
    @SequenceGenerator(name = "products_sequence", sequenceName = "products_sequence")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "products_sequence")
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false, length = 100)
    private String description;

    @Column(nullable = false)
    private boolean available;


    @OneToOne(cascade = CascadeType.ALL)
    @MapsId
    @JoinColumn(name = "photo_id")
    private PhotoDetails photo;

    public Product(String name, String description, boolean available, PhotoDetails photo) {
        this.name = name;
        this.description = description;
        this.available = available;
        this.photo = photo;
    }
}
