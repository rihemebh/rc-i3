import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginDTO } from 'src/app/dtos/login.dto';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  error = "";
  loading = false;
  constructor( private formBuilder: FormBuilder, private authService : AuthService, private router: Router) {}

  loginForm = this.formBuilder.group({
   email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(8)]],
});

 onSubmit(){
  this.error  = "";
  this.loading =true;
  console.log(this.loginForm.value)
  var login =  new LoginDTO(this.loginForm.value.email, this.loginForm.value.password);  

  this.authService.login(login).subscribe( 
    { next: (response)=>{
      console.log(response);
      localStorage.setItem('token', response.token);
      localStorage.setItem('username', response.username);
      this.router.navigate(['/products'])
      this.loading =false;
    },
      error: (error)=> {console.log(error);
        if(error.status == "403") this.error = "Wrong credentials, try again!";
        this.loading =false;}
  })
 }
  ngOnInit() {
    if(localStorage.getItem('token')!=null)
    this.router.navigate(['/products'])
  }
  ngOnDestroy() {
  }

}
