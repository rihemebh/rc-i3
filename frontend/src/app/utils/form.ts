export const form = {
    name : {
        name: "name",
        type: "text", 
        label: "Name :",
        labels: [],
        placeholder:"Add the product name here",
        constraints: ["required"],
        value: ""
    },
    description: {
        name: "description",
        type: "text", 
        label: "Description :",
        labels: [],
        placeholder:"Add the product description here",
        constraints: ["required"],
       
        value: ""
    },
    available: {
        name: "available-radio",
        type: "radio", 

        labels: ["Available", "Not Available"],
        placeholder:"",
        constraints: ["required"],
        value: ""
    },


    photo:{
        name: "photo",
        type: "file",
        labels: [],
        label: "Photo :",
        placeholder:"",
        constraints: ["required"],
        value :""

    }
}