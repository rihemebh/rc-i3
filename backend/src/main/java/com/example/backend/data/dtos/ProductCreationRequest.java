package com.example.backend.data.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ProductCreationRequest {

    private String name;
    private String description;
    private boolean available;
    private MultipartFile file;
}
