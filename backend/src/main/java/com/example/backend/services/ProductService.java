package com.example.backend.services;


import com.example.backend.data.dtos.ProductCreationRequest;
import com.example.backend.data.models.PhotoDetails;
import com.example.backend.data.models.Product;
import com.example.backend.repositories.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {

    final private ProductRepository repository;
    public Product create(ProductCreationRequest request) throws IOException {
        MultipartFile file = request.getFile();
        PhotoDetails photo = new PhotoDetails();
        photo.setFileName(file.getOriginalFilename());
        photo.setFileType(file.getContentType());
        photo.setGrpData(file.getBytes());
        Product product = new Product(request.getName(), request.getDescription(), request.isAvailable(), photo);
        return this.repository.save(product);
    }

    public Product get(Long id) {
        return this.repository.getReferenceById(id);
    }

    public List<Product> getAll() { return this.repository.findAll();
    }
}
