import { Base_URL } from '../utils/constants';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginDTO } from '../dtos/login.dto';

@Injectable({providedIn: 'root'})
export class AuthService {
    constructor(private httpClient: HttpClient) { }
    

     login(credentials : LoginDTO): Observable<any>{

      return  this.httpClient.post<any>(`${Base_URL}/auth/login`, credentials);

    }

}