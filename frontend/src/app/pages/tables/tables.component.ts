import { AdminService } from './../../services/admin.service';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer } from '@angular/platform-browser';
import { buffer } from 'stream/consumers';
@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.scss']
})
export class TablesComponent implements OnInit {

  forms = [

  ]
  constructor(private modalService: NgbModal, private adminService: AdminService, private sanitizer: DomSanitizer) {}
   
	 open(content) {
   
		this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
			(result) => {
				console.log(`Closed with: ${result}`);
			}
		
		);
	}

  ngOnInit() {

    this.adminService.getData().subscribe({
        next: (response)=>{
          this.forms = response;
          this.forms = this.forms.map((form)=>{
          var image = 'data:image/jpeg;base64,' + form.photo.grpData;
            return {...form, picture: image}
          })     
        },
        error: (error)=> console.log(error)
      
    })
  }

}
