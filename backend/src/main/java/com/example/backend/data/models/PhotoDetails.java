package com.example.backend.data.models;


import jakarta.persistence.*;
import lombok.Data;

@Entity(name = "photos")
@Data
public class PhotoDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private long id;

    @Column
    private String fileType;
    @Column
    private String fileName;

    @Column
    private byte[] grpData;




}
