package com.example.backend.controllers;

import com.example.backend.data.dtos.ProductCreationRequest;
import com.example.backend.data.models.Product;
import com.example.backend.services.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/v1/products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService service;

    @CrossOrigin(origins = "http://localhost:8080")
    @PostMapping()
    public ResponseEntity<Product> createProduct(@ModelAttribute ProductCreationRequest request) throws IOException {
       return ResponseEntity.ok( this.service.create(request));

    }
    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/{id}")
    public ResponseEntity<Product> getProduct( @PathVariable long id){
        return ResponseEntity.ok( this.service.get(id));
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping()
    public ResponseEntity<List<Product>> getProducts(){
        return ResponseEntity.ok( this.service.getAll());
    }
}
