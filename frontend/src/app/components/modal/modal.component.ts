
import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as f from '../../utils/form';
@Component({

    selector: 'app-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ModalComponent implements OnInit  {
    closeResult = '';
    @Input() title;
    @Input() data = {
        name: "name 1",
        description: "desc",
        available: true,
        photo:"photo.jpg"
    };
 formModel = f.form;
 model  =[];
 form : any;

    
    constructor(private modalService: NgbModal, private formBuilder: FormBuilder,) {


    }
    ngOnInit(): void {

        
        var builder = {};
        var validator = [];

        for(let key in this.formModel){
            validator.push(Validators[this.formModel[key].constraints[0]]);
            builder[key] = [ this.data[key], validator];
            if(this.formModel[key]['labels'].length > 0){

                for(let i = 0; i< this.formModel[key]['labels'].length ; i++){
                    this.model.push({...this.formModel[key], label : this.formModel[key]['labels'][i]  })
                }
                
            }else{
                this.model.push(this.formModel[key]);
            }
            
    
            validator = [];
        }

        console.log(this.model);
              
        this.form = this.formBuilder.group(builder);


     
    }
   
	 open(content) {
   
		this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' , windowClass: 'zindex',  size: 'xl'}).result.then(
			(result) => {
				this.closeResult = `Closed with: ${result}`;
			},
			(reason) => {
				this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
			},
		);
	}

	private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
			return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
			return 'by clicking on a backdrop';
		} else {
			return `with: ${reason}`;
		}
	}

}