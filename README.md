# Overview
[ This application is a part of Vneuron technical test. ]

A simple admin dashboard where you can login, check the list of products and modify them.

You will find in this file an overview about the architceture, the system design, a small demo and instructions on how to test the application. 
# Tools

- Angular
- Springboot 3
- Docker 
- Postgresql
- PgAdmin / DataGrip
- Intellij 
- VsCode
# System Design 


<img src="pictures/system-design.png" >

# Architecture: 3-Tier

<img src="pictures/application-architecture.png" >

## Presentation Tier 

This Tier represents the Frontend of the application where you will find the different UI screens, and the corredpond front logic. 

- Developed using Angular 
- Follow MVVM architecture: seperating the UI from the logic

## Logic Tier 

This Tier represents the Backend of the application That provides the REST APIs used by the frontend and the access to the database logic.

It is developed using springboot3 that follows the layered architecture.
You will find in this picture the different layers used in this application.

<img src="pictures/backend-architecture.png" >

- Project structure according to the architecture

```
        .
├── Controllers/
│   ├── AuthenticationController
│   └── ProductController
├── Data/
│   ├── Dtos/
│   │   ├── AuthenticationRequest
│   │   ├── AuthenticationResponse
│   │   ├── ProductCreactionRequest
│   │   ├── RegisterRequest
│   │   └── UploadImageRequest
│   └── Models/
│       ├── Product
│       ├── PhotoDetails
│       └── User
├── Repositories/
│   ├── UserRepository
│   └── ProductRepository
├── Security/
│   ├── ApplcationConfig # Contains beans 
│   ├── JWTFilter
│   ├── JWTUtil
│   └── SecurityConfig
└── Services/
    ├── AuthenticationService
    └── ProdcductService
```
    
 - This project follows the best practices of creating Rest APIs
 - 
   - Versioning
   - using nouns
   - meaningful names
   - using the right HTTP requests
   - ...


# Data Tier

 The data of the application is stored in an SQL database.

The docker-compose.yaml creates 2 containers :

- postgres for the db
- pgadmin for the visualization

# Demo

## Login page 
 
<img src="pictures/login-page.png" >

### Form Validation

<img src="pictures/loginpage-emailformat.png" >

<img src="pictures/loginpage-required%20.png" >

## Dashboard

<img src="pictures/dashboard.png" >

### Edit form 

<img src="pictures/edit-modal.png" >

### Logout
<img src="pictures/logout.png" >

# Test the app 

- Make sure that you installed all the tools 
- In the main folder run this command to run docker containers : ``docker compose up -d``
- In The backend folder: 
    - if you are using an IDE you can simply run the app via the button 
    - or run : ``mvn spring-boot:run``
- In the frontend folder run : ``ng serve``

